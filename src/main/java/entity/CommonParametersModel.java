package entity;

import java.io.Serializable;

public class CommonParametersModel implements Serializable {

	private static final long serialVersionUID = 8728608970717224766L;

	private String databasePool = "HikariCP";

	private String ifUseSwagger = "是";

	public String getDatabasePool() {
		return databasePool;
	}

	public void setDatabasePool(String databasePool) {
		this.databasePool = databasePool;
	}

	public String getIfUseSwagger() {
		return ifUseSwagger;
	}

	public void setIfUseSwagger(String ifUseSwagger) {
		this.ifUseSwagger = ifUseSwagger;
	}



}
