package codeMaker;

import codeMaker.impl.MakeCodeImpl;
import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.Constant;
import entity.Parameters;
import org.apache.commons.lang3.StringUtils;
import util.CodeWriterUtil;
import util.ConfigUtil;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;
import java.util.Set;

public class MakeCode extends JFrame {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	protected JFrame frmv;
	protected JTextField projectName;
	protected JComboBox<String> framework;
	protected JComboBox<String> databaseType;
	protected JTextField dataBaseIp;
	protected JTextField dataBasePort;
	protected JTextField dataBasePwd;
	protected JTextField dataBaseName;
	protected JTextField dataBaseUserName;
	protected JTextField outPath;
	protected JTextField tableName;
	// 2021-11-24 生成模块选择
	protected JCheckBox controllerBox = new JCheckBox("controller");
	protected JCheckBox serviceBox = new JCheckBox("service");
	protected JCheckBox daoBox = new JCheckBox("dao");
	protected JCheckBox entityBox = new JCheckBox("entity");
	protected JCheckBox viewBox = new JCheckBox("view");
	protected JCheckBox isAllBox = new JCheckBox("全部");
	// 2021-12-03 数据源配置
	protected JComboBox<String> dataSouceBox = new JComboBox<>();
	protected JCheckBox isMutiDataSource = new JCheckBox("多数据源模式");
	protected JButton dataSourceBtn = new JButton("数据源配置");
	protected JComboBox<String> loginConfig = new JComboBox<>();
	protected JCheckBox authorityCheckBox = new JCheckBox("添加权限管理");
	protected JButton moneyBtn = new JButton("<html><body color='red'><b>赞&emsp;赏</b></body></html>");
	// 2022-03-17
	protected JCheckBox cloudModelCheckBox = new JCheckBox("SpringCloud模式");
	//2022-04-01
	protected JComboBox<String> themeComboBox = new JComboBox<>();
	protected JTextField proCnField;
	//2022-04-02
	protected JLabel projectNameLabel = new JLabel("项目英文名称");
	protected JLabel proCnLabel = new JLabel("项目中文名称");
	protected JCheckBox redisCheckBox = new JCheckBox("集成redis单点登录");
	protected JCheckBox coverCheckBox = new JCheckBox("覆盖已存在文件");
	protected JLabel cloudSysDsLabel = new JLabel("系统服务数据源");
	protected JComboBox<String> cloudSysDsComboBox = new JComboBox<>();

	/**
	 * Launch the application.
	 */
	// SubstanceCeruleanLookAndFeel 蓝色简约
	// SubstanceCremeCoffeeLookAndFeel 咖啡俏皮
	// SubstanceGeminiLookAndFeel 黑绿简约
	// SubstanceGraphiteLookAndFeel 灰黑控
	// SubstanceModerateLookAndFeel 白色简约
	// SubstanceOfficeBlack2007LookAndFeel office2007
	// SubstanceBusinessBlackSteelLookAndFeel 商务黑
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				// UIManager.setLookAndFeel(com.jtattoo.plaf.mcwin.McWinLookAndFeel.class.getName());
				// UIManager.setLookAndFeel(org.pushingpixels.substance.api.skin.SubstanceGraphiteLookAndFeel.class.getName());
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);
				// 调试界面放开用
				//MakeCode window = new MakeCode();
				MakeCode window = MakeCodeImpl.getInstance();
				window.initialize();
				Constant.frmv = window.frmv;
				window.frmv.setVisible(true);
				File ipFile = new File(Constant.gitIpAdressFilePath);
				if (!ipFile.exists()) {
					JOptionPane.showMessageDialog(window.frmv,
							"代码生成器客户端有重大更新，请前往公众号“螺旋编程极客”或网址 http://www.zrxlh.top:8088/coreCode/ 下载最新版方可使用，带来不便，深表歉意！",
							"提示", JOptionPane.WARNING_MESSAGE);
					System.exit(0);
				}
				File gitConfigFile = new File(Constant.gitConfigFilePath);
				if (!gitConfigFile.exists()) {
					JOptionPane.showMessageDialog(window.frmv,
							"代码生成器客户端更新啦，优化了资源拉取速度，在 http://www.zrxlh.top:8088/coreCode/ 下载最新版方可使用！", "提示",
							JOptionPane.WARNING_MESSAGE);
				}
				// 更新提示
				upTip(window.frmv);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "启动时产生未知错误（可截图联系作者解决）：" + e.getMessage(), "错误",
						JOptionPane.ERROR_MESSAGE);
			}
		});
	}

	/**
	 * Create the application.
	 */
	protected MakeCode() {
		// 调试界面放开用
		//initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// 调试界面放开用
		//Parameters configParameters = new Parameters();
		// 读取上次的配置
		Parameters configParameters = ConfigUtil.readConfig();
		Parameters parameters = new Parameters();
		parameters.setCloudSysName(StringUtils.isBlank(configParameters.getCloudSysName()) ? "cloud" : configParameters.getCloudSysName());
		parameters.setCloudSysEngName(StringUtils.isBlank(configParameters.getCloudSysEngName()) ? "cloud" : configParameters.getCloudSysEngName());
		parameters.setCloudNeteWork(StringUtils.isBlank(configParameters.getCloudNeteWork()) ? "gateway" : configParameters.getCloudNeteWork());
		parameters.setCloudRegiseterCenter(StringUtils.isBlank(configParameters.getCloudRegiseterCenter()) ? "eureka" : configParameters.getCloudRegiseterCenter());
		parameters.setOnlyCloudModel(configParameters.isOnlyCloudModel());
		frmv = new JFrame();
		frmv.setIconImage(Toolkit.getDefaultToolkit().getImage(MakeCode.class.getResource(
				"/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
		frmv.setResizable(true);
		frmv.setTitle("代码生成器V2.35 by 小螺旋丸");
		frmv.setBounds(100, 100, 772, 810);
		frmv.setLocationRelativeTo(null);
		frmv.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		projectName = new JTextField();
		projectName.setColumns(10);
		projectName.setText(configParameters.getProjectNameVal());
		proCnField = new JTextField();
		proCnField.setColumns(10);
		proCnField.setText(StringUtils.isBlank(configParameters.getProCnNameVal()) ? configParameters.getProjectNameVal() : configParameters.getProCnNameVal());
		//2022-03-17
		cloudModelCheckBox.setSelected(configParameters.isCloudModel());
		JLabel frmaeWorkLabel = new JLabel("后台框架");
		framework = new JComboBox<>();
		framework.setModel(new DefaultComboBoxModel<>(new String[]{"springBoot", "ssm"}));
		framework.setSelectedItem(configParameters.getFrameWorkVal());
		JLabel databaseTypeLabel = new JLabel("数据库类型");
		databaseType = new JComboBox<>();
		databaseType.setModel(new DefaultComboBoxModel<>(new String[]{"", "mysql", "postgresql", "oracle", "sqlserver"}));
		databaseType.setSelectedItem(configParameters.getDataBaseTypeVal());
		JLabel databaseIpLabel = new JLabel("数据库IP");
		dataBaseIp = new JTextField();
		dataBaseIp.setColumns(10);
		dataBaseIp.setText(configParameters.getDataBaseIpVal());
		JLabel databasePortLabel = new JLabel("端口");
		dataBasePort = new JTextField();
		dataBasePort.setColumns(10);
		dataBasePort.setText(configParameters.getDataBasePortVal());
		JLabel portRequired = new JLabel("*");
		portRequired.setForeground(Color.RED);
		JLabel databasePwdLabel = new JLabel("密码");
		dataBasePwd = new JTextField();
		dataBasePwd.setColumns(10);
		dataBasePwd.setText(configParameters.getDataBasePwdVal());
		JLabel pwdRequired = new JLabel("*");
		pwdRequired.setForeground(Color.RED);
		JLabel databasePwdRequired = new JLabel("*");
		databasePwdRequired.setForeground(Color.RED);
		JLabel databaseNameLable = new JLabel("数据库名称（SERVICE NAME/SID）");
		dataBaseName = new JTextField();
		dataBaseName.setColumns(10);
		dataBaseName.setText(configParameters.getDataBaseNameVal());
		JLabel dataBaseNameRequired = new JLabel("*");
		dataBaseNameRequired.setForeground(Color.RED);
		JLabel userNameLabel = new JLabel("用户名");
		dataBaseUserName = new JTextField();
		dataBaseUserName.setColumns(10);
		dataBaseUserName.setText(configParameters.getDataBaseUserNameVal());
		JLabel userNameRequired = new JLabel("*");
		userNameRequired.setForeground(Color.RED);
		JLabel outPathLabel = new JLabel("项目输出路径");
		outPath = new JTextField();
		outPath.setEditable(false);
		outPath.setColumns(10);
		outPath.setText(configParameters.getOutPathVal());
		JLabel outPathRequired = new JLabel("*");
		outPathRequired.setForeground(Color.RED);
		JButton button = new JButton("选择");
		// 如果是MAC_OS系统
		File desktopDir = FileSystemView.getFileSystemView().getHomeDirectory();
		String desktopPath = desktopDir.getAbsolutePath();
		if (Constant.MAC_OS.equals(CodeWriterUtil.getSystemType())) {
			this.outPath.setText(desktopPath + "/Desktop");
			button.setEnabled(false);
			button.setVisible(false);
		} else {
			this.outPath.setText(desktopPath);
		}
		MakeCodeImpl.getInstance().setPathChooseListener(button);

		JLabel loginConfigLabel = new JLabel("登录配置");
		loginConfig.setModel(new DefaultComboBoxModel<>(new String[]{"静态用户", "动态用户"}));
		loginConfig.setSelectedItem(configParameters.getMakeModelVal());
		MakeCodeImpl.getInstance().setLoginConfigListener();

		final JButton btnNewButton = new JButton("点击生成");
		JLabel tableNamesLabel = new JLabel("数据表（多个用 # 隔开）");
		tableName = new JTextField();
		tableName.setColumns(10);
		tableName.setText(configParameters.getTableName());
		JLabel clientStyleLabel = new JLabel("前台风格");
		final JComboBox<String> clientStyle = new JComboBox<>();
		clientStyle.setModel(new DefaultComboBoxModel<>(
				new String[]{"custom-经典", "flatly-平淡", "journal-杂志", "lumen-立体", "paper-纸质", "readable-书本"}));
		clientStyle.setSelectedItem(configParameters.getClientFrameWorkVal());
		// 生成代码的提示性文字
		JLabel tips = new JLabel("点击生成");
		tips.setText("");
		tips.setFont(new Font("宋体", Font.BOLD, 15));
		JLabel jsFrameWorkLabel = new JLabel("JS框架");
		JComboBox<String> jsFrameWorkComBox = new JComboBox<>();
		jsFrameWorkComBox.setModel(new DefaultComboBoxModel<>(new String[]{"vue", "jquery"}));
		jsFrameWorkComBox.setSelectedItem(configParameters.getJsFrameWork());
		JLabel themeLable = new JLabel("主题设置");
		themeComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"前后端分离响应式", "经典后台Thymleaf版"}));
		themeComboBox.setSelectedItem(configParameters.getThemeVal());

		// 20210518 添加权限管理
		authorityCheckBox.setSelected(configParameters.isAuthority());
		if (authorityCheckBox.isSelected()) {
			loginConfig.setEnabled(false);
		}
		MakeCodeImpl.getInstance().setAuthListener();

		controllerBox.setSelected(configParameters.isController());
		serviceBox.setSelected(configParameters.isService());
		daoBox.setSelected(configParameters.isDao());
		entityBox.setSelected(configParameters.isEntity());
		viewBox.setSelected(configParameters.isView());
		isAllBox.setSelected(!configParameters.isController() && !configParameters.isService()
				&& !configParameters.isDao() && !configParameters.isEntity() && !configParameters.isView());
		redisCheckBox.setSelected(configParameters.isRedisSingleLogin());
		JLabel makeMtLabel = new JLabel("生成方式");
		coverCheckBox.setSelected(configParameters.isCoverExitFile());

		// 设置生代码的listener
		MakeCodeImpl.getInstance().setMakeCodeListener(parameters, btnNewButton, clientStyle, tips, jsFrameWorkComBox);

		JButton databaseConfig = new JButton("数据项配置");
		// 设置数据库配置的listener
		MakeCodeImpl.getInstance().setDataBaseConfigListener(tips, databaseConfig);

		JMenuBar menuBar = new JMenuBar();
		frmv.setJMenuBar(menuBar);
		JMenu highConfig = new JMenu("高级配置");
		menuBar.add(highConfig);
		JMenuItem tablesQuery = new JMenuItem("多表联查配置");
		MakeCodeImpl.getInstance().setTablesQueryListener(tablesQuery);

		JMenuItem loginItem = new JMenuItem("登录功能定制");
		MakeCodeImpl.getInstance().setLoginListener(loginItem);
		highConfig.add(loginItem);

		// 自定义实体
		JMenuItem makeEntity = new JMenuItem("自定义实体");
		MakeCodeImpl.getInstance().setMakeEntityListener(makeEntity);

		highConfig.add(makeEntity);
		highConfig.add(tablesQuery);

		JMenuItem commonParamsMenu = new JMenuItem("常用参数配置");
		MakeCodeImpl.getInstance().setCommonParamListener(commonParamsMenu);
		highConfig.add(commonParamsMenu);

		JMenuItem cloudMenu = new JMenuItem("SpringCloud配置");
		MakeCodeImpl.getInstance().setCloudListener(cloudMenu, parameters);
		highConfig.add(cloudMenu);

		JMenuItem usualModel = new JMenuItem("常用模块配置");
		MakeCodeImpl.getInstance().setUsualListener(usualModel);
		highConfig.add(usualModel);

		JMenu utilMenu = new JMenu("实用工具");
		menuBar.add(utilMenu);
		// 只有windows系统才有everything
		if (Constant.WINDOWS.equals(CodeWriterUtil.getSystemType())) {
			JMenuItem mntmEverything = new JMenuItem("everything");
			MakeCodeImpl.getInstance().setMenuEveryThingListener(mntmEverything);
			utilMenu.add(mntmEverything);
		}
		JMenuItem mntmDubbo = new JMenuItem("dubbo脚手架");
		MakeCodeImpl.getInstance().setDubboListener(mntmDubbo);
		utilMenu.add(mntmDubbo);

		JMenuItem mntmSpringcloud = new JMenuItem("springcloud脚手架（旧版）");
		MakeCodeImpl.getInstance().setSpringCloudListener(mntmSpringcloud);
		utilMenu.add(mntmSpringcloud);

		JMenu versionUpdate = new JMenu("版本特性");
		menuBar.add(versionUpdate);

		JMenuItem menuItem = new JMenuItem("查看本次更新");
		menuItem.addActionListener(e -> upTip(frmv));
		versionUpdate.add(menuItem);

		JMenuItem giveMontyItem = new JMenuItem("打赏作者");
		MakeCodeImpl.getInstance().setGiveMoneyListener(giveMontyItem);
		versionUpdate.add(giveMontyItem);

		// 2021-11-23
		JLabel modelChooseLabel = new JLabel("生成模块选择");
		MakeCodeImpl.getInstance().setSelectModelBoxListener();

		// 2021-12-03
		JLabel mainDataSourceLabel = new JLabel("选择主数据源");
		MakeCodeImpl.getInstance().setDataBaseSouceConfigListener();
		MakeCodeImpl.getInstance().setMoneyBtnListener();

		//2022-04-01
		MakeCodeImpl.getInstance().setCloudBoxListener();

		MakeCodeImpl.getInstance().setRedisCheckBoxListener();

		//系统服务数据源
		cloudSysDsLabel.setVisible(false);
		cloudSysDsComboBox.setVisible(false);

		databaseType.setEnabled(false);
		dataBaseIp.setEnabled(false);
		dataBasePort.setEnabled(false);
		dataBasePwd.setEnabled(false);
		dataBaseName.setEnabled(false);
		dataBaseUserName.setEnabled(false);
		tableName.setEnabled(false);
		DefaultComboBoxModel<String> sourceModel = new DefaultComboBoxModel<>(new String[]{CodeConstant.PLEASE_CHOOSE});
		dataSouceBox.setModel(sourceModel);
		//cloud系统服务
		DefaultComboBoxModel<String> sourceCloudModel = new DefaultComboBoxModel<>(new String[]{CodeConstant.PLEASE_CHOOSE});
		cloudSysDsComboBox.setModel(sourceCloudModel);
		Set<String> sources = ChildWindowConstant.dataSourceModelMap.keySet();
		if (!sources.isEmpty()) {
			sources.forEach(s -> {
				sourceModel.addElement(s);
				sourceCloudModel.addElement(s);
			});
		}
		dataSouceBox.setSelectedItem(StringUtils.isEmpty(configParameters.getDataSourceName()) ? CodeConstant.PLEASE_CHOOSE : configParameters.getDataSourceName());
		//设置cloud系统服务
		cloudSysDsComboBox.setSelectedItem(configParameters.getCloudSysDsParam() == null ? CodeConstant.PLEASE_CHOOSE : configParameters.getCloudSysDsParam().getDataSourceName());
		isMutiDataSource.setSelected(configParameters.isMutiDataSource());
		MakeCodeImpl.getInstance().setMutiDataSoucreBoxListener();
		MakeCodeImpl.getInstance().setDataSourceBoxListener();

		JLabel lblNewLabel = new JLabel("<html><body>此工具可以根据配置灵活生成从前台到后台的所有代码！<br/><br/>\r不断升级中，欢迎提出您的宝贵意见！<br/><br/>\r有问题在公众号底部菜单加我微信反馈哦！</body></html>");

		//2022-04-02
		if (configParameters.isCloudModel()) {
			MakeCodeImpl.getInstance().setCloudParam();
		}

		moneyBtn.setVisible(false);

		// 样式
		GroupLayout groupLayout = new GroupLayout(frmv.getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(33)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
																		.addGroup(groupLayout.createSequentialGroup()
																				.addComponent(outPathLabel)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(outPath, GroupLayout.PREFERRED_SIZE, 344, GroupLayout.PREFERRED_SIZE)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(outPathRequired)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(button))
																		.addGroup(groupLayout.createSequentialGroup()
																				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
																						.addComponent(themeLable)
																						.addComponent(clientStyleLabel))
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																						.addComponent(clientStyle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																						.addComponent(themeComboBox, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
																				.addGap(30)
																				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(modelChooseLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(controllerBox))
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(jsFrameWorkLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(jsFrameWorkComBox, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)))
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
																						.addGroup(groupLayout.createSequentialGroup()
																								.addGap(27)
																								.addComponent(loginConfigLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(loginConfig, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(serviceBox)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(daoBox)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(entityBox)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(viewBox)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(isAllBox))))
																		.addGroup(groupLayout.createSequentialGroup()
																				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(databaseIpLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataBaseIp, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(databasePwdRequired)
																								.addGap(46)
																								.addComponent(databasePortLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataBasePort, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(portRequired))
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(databaseNameLable)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataBaseName, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataBaseNameRequired))
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(mainDataSourceLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataSouceBox, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataSourceBtn, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)))
																				.addGap(60)
																				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																						.addGroup(groupLayout.createSequentialGroup()
																								.addComponent(userNameLabel)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(dataBaseUserName, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
																								.addPreferredGap(ComponentPlacement.RELATED)
																								.addComponent(userNameRequired))
																						.addGroup(groupLayout.createSequentialGroup()
																								.addGap(10)
																								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
																										.addComponent(moneyBtn, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
																										.addGroup(groupLayout.createSequentialGroup()
																												.addComponent(databasePwdLabel)
																												.addPreferredGap(ComponentPlacement.RELATED)
																												.addComponent(dataBasePwd, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
																												.addPreferredGap(ComponentPlacement.RELATED)
																												.addComponent(pwdRequired))))))
																		.addGroup(groupLayout.createSequentialGroup()
																				.addComponent(tableNamesLabel)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(tableName, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(databaseConfig)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(authorityCheckBox)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(cloudSysDsLabel)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(cloudSysDsComboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																		.addGroup(groupLayout.createSequentialGroup()
																				.addComponent(databaseTypeLabel)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(databaseType, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(isMutiDataSource)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(redisCheckBox))
																		.addGroup(groupLayout.createSequentialGroup()
																				.addComponent(makeMtLabel)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(coverCheckBox)))
																.addGap(117))
														.addGroup(groupLayout.createSequentialGroup()
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																		.addGroup(groupLayout.createSequentialGroup()
																				.addComponent(frmaeWorkLabel, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(framework, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
																				.addPreferredGap(ComponentPlacement.RELATED)
																				.addComponent(cloudModelCheckBox))
																		.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
																				.addGroup(groupLayout.createSequentialGroup()
																						.addComponent(proCnLabel)
																						.addPreferredGap(ComponentPlacement.RELATED)
																						.addComponent(proCnField))
																				.addGroup(groupLayout.createSequentialGroup()
																						.addPreferredGap(ComponentPlacement.RELATED)
																						.addComponent(projectNameLabel)
																						.addPreferredGap(ComponentPlacement.RELATED)
																						.addComponent(projectName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
																.addGap(72)
																.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 317, GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(ComponentPlacement.RELATED, 95, Short.MAX_VALUE))))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(270)
												.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(175)
												.addComponent(tips, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE)))
								.addContainerGap())
		);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(11)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(proCnLabel)
														.addComponent(proCnField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGap(16)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(projectNameLabel)
														.addComponent(projectName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGap(18)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(frmaeWorkLabel)
														.addComponent(framework, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(cloudModelCheckBox))
												.addGap(24)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(mainDataSourceLabel)
														.addComponent(dataSouceBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(dataSourceBtn)))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(2)
												.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(moneyBtn, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(databaseTypeLabel)
										.addComponent(databaseType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(isMutiDataSource)
										.addComponent(redisCheckBox))
								.addGap(21)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(databaseIpLabel)
										.addComponent(dataBaseIp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(databasePortLabel)
										.addComponent(dataBasePort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(portRequired)
										.addComponent(databasePwdRequired)
										.addComponent(databasePwdLabel)
										.addComponent(dataBasePwd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(pwdRequired))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(databaseNameLable)
										.addComponent(dataBaseName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(dataBaseNameRequired)
										.addComponent(userNameLabel)
										.addComponent(dataBaseUserName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(userNameRequired, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(tableNamesLabel)
										.addComponent(tableName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(databaseConfig)
										.addComponent(authorityCheckBox)
										.addComponent(cloudSysDsLabel)
										.addComponent(cloudSysDsComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(outPathLabel)
										.addComponent(outPath, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(outPathRequired)
										.addComponent(button))
								.addGap(27)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(clientStyleLabel)
										.addComponent(clientStyle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(jsFrameWorkLabel)
										.addComponent(jsFrameWorkComBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(loginConfig, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(loginConfigLabel))
								.addGap(27)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(themeLable)
										.addComponent(themeComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(modelChooseLabel)
										.addComponent(controllerBox)
										.addComponent(serviceBox)
										.addComponent(daoBox)
										.addComponent(entityBox)
										.addComponent(viewBox)
										.addComponent(isAllBox))
								.addGap(26)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE, false)
										.addComponent(makeMtLabel)
										.addComponent(coverCheckBox))
								.addGap(26)
								.addComponent(tips)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(btnNewButton)
								.addContainerGap(181, Short.MAX_VALUE))
		);
		frmv.getContentPane().setLayout(groupLayout);
	}

	private static void upTip(JFrame frmv) {
		JOptionPane.showMessageDialog(frmv,
				"本次更新：" + CodeConstant.NEW_LINE + CodeConstant.NEW_LINE + "1.生成优化，添加项目中文名称和生成覆盖方式选择！" + CodeConstant.NEW_LINE
						+ CodeConstant.NEW_LINE + "2.添加springCloud模式生成，目前注册中心支持eureka，nacos，网关支持gateway，zuul，自动生成feign服务调用模块，后续添加限流组件选择！"
						+ CodeConstant.NEW_LINE + CodeConstant.NEW_LINE + "3.集成redis单点登录支持"
						+ CodeConstant.NEW_LINE + CodeConstant.NEW_LINE + "后续更新：vue原生页面，常用模块组件生成！"
						+ CodeConstant.NEW_LINE + CodeConstant.NEW_LINE + "敬请期待！",
				"提示", JOptionPane.INFORMATION_MESSAGE);
	}
}
