#注册中心应用名称
spring:
  application:
    name: eureka
server:
  port: 8001

eureka:
  server:
    use-read-only-response-cache: false
    # 自我保护机制
    enable-self-preservation: true
  instance:
    hostname: 127.0.0.1
    prefer-ip-address: false
  client:
    #是否检索服务
    fetch-registry: false
    # 是否向eureka注册自身服务
    register-with-eureka: false
    #服务注册地址
    service-url:
      defaultZone: http://<#noparse>${</#noparse>eureka.instance.hostname}:<#noparse>${</#noparse>server.port}/eureka/

