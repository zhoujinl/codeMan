package gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
<#if cloudRegisteCenter=="eureka">
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
</#if>

<#if cloudRegisteCenter=="eureka">
@EnableEurekaClient
</#if>
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
