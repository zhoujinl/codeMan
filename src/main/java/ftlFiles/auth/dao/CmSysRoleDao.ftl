package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysRoleButtonEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysRoleEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysRoleMenuEntity;

import java.util.List;

@Mapper
@Repository
public interface CmSysRoleDao extends BaseDao<CmSysRoleEntity> {

	/**
	 * 查询全部
	 *
	 * @return
	 */
	List<CmSysRoleEntity> listAll();

	/**
	 * 根据id查询
	 *
	 * @param roleId
	 * @return
	 */
	CmSysRoleEntity getById(@Param("roleId") Long roleId);

	/**
	 * 根据roleId删除相关菜单权限
	 *
	 * @param roleId
	 */
	void delMenuByRoleId(@Param("roleId") Long roleId);

	/**
	 * 根据roleId删除相关按钮权限
	 *
	 * @param roleId
	 */
	void delButtonByRoleId(@Param("roleId") Long roleId);

	/**
	 * 添加roleButton表
	 * @param cmSysRoleButtonEntity
	 */
	void addRoleButton(CmSysRoleButtonEntity cmSysRoleButtonEntity);

	/**
	 * 添加roleMenu表
	 * @param cmSysRoleMenuEntity
	 */
	void addRoleMenu(CmSysRoleMenuEntity cmSysRoleMenuEntity);
}
