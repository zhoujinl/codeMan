package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant.YesOrNo;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dto.NodeMoveDto;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysMenuEntity;

import java.util.List;

/**
 * @author zrx
 */
public interface CmSysMenuService {

	void add(CmSysMenuEntity entity);

	void delete(CmSysMenuEntity entity);

	void update(CmSysMenuEntity entity);

	/**
	 * 查询全部节点
	 *
	 * @return
	 */
	List<CmSysMenuEntity> listAll(Long roleId, YesOrNo isAll);

	/**
	 * 移动节点
	 *
	 * @param nodeMoveDto
	 */
	CmSysMenuEntity removeNode(NodeMoveDto nodeMoveDto);

	/**
	 * 查看节点是否可移动
	 * @param nodeMoveDto
	 * @return
	 */
	boolean canMove(NodeMoveDto nodeMoveDto);
}
