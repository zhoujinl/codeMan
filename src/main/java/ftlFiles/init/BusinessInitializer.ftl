package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.init;

<#if isRedisSingleLogin>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.RedisCacheUtil;
</#if>
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
<#if isRedisSingleLogin>
import org.springframework.data.redis.core.RedisTemplate;
</#if>
import org.springframework.stereotype.Component;

/**
 * 业务初始化器，可在此处做一些项目启动的初始化操作
 *
 */
@Slf4j
@Component
public class BusinessInitializer implements ApplicationRunner {

<#if isRedisSingleLogin>
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
</#if>
	@Override
	public void run(ApplicationArguments args) {
<#if isRedisSingleLogin>
		RedisCacheUtil.init(redisTemplate);
</#if>
	}
}
